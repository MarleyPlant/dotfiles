#!/bin/bash

# --- Snap Store ---
install_apt_packages snapd

allpackages=$(get_array_from_json ./configs/applications.json ".snap[]")
packages=($(multiselect "$allpackages" "SNAP Packages"))

for package in "${packages[@]}"; do
    isClassic=$(echo $package | jq -r '.classic')
    packageName=$(echo $package | jq -r '.package')
    if [ "$isClassic" = "true" ]; then
        sudo snap install --classic $packageName
    else
        sudo snap install $packageName
    fi
done
