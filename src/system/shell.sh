sudo apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo apt install -y fonts-powerline

FILE=/usr/local/bin/starship
if [ -f "$FILE" ]; then
    echo "Starship Already Installed"
else
    curl -sS https://starship.rs/install.sh | sh
fi