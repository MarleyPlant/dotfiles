#!/bin/bash
source ./src/util.sh

# # --- Copy Configs ---

### STARSHIP
STARSHIPSHELL=/usr/local/bin/starship

if [ -f "$STARSHIPSHELL" ]; then
    echo "Starship Already Installed"
else
    curl -fsSL https://starship.rs/install.sh | bash
fi;

### ZSH
if [ -f "~/.zshrc" ]; then
    echo "ZSHRC Already Installed"
else
    cp $DTFCfgDir/.zshrc ~/
fi;

## POP OS FAVORITES?
DOCKICONS=('firefox.desktop', 'discord_discord.desktop', 'thunderbird.desktop', 'obsidian_obsidian.desktop', 'tabby.desktop', 'projectsfolder.desktop', 'code_code.desktop',)
AVAILABLEICONS="["

for icon in "${DOCKICONS[@]}"; do
    find /usr/share/applications/ /var/lib/snapd/desktop/applications/ | grep -q ${icon}
    if [[ $? -eq 1 ]]; then
        icon=${icon::-1}
        AVAILABLEICONS+="'${icon}',"
    fi
done

AVAILABLEICONS=${AVAILABLEICONS::-1}
AVAILABLEICONS+=", 'org.gnome.Nautilus.desktop']"

dconf write /org/gnome/shell/favorite-apps "${AVAILABLEICONS}"
echo "Dock Icons Updated to: ${AVAILABLEICONS}"

## Wallpaper?
## Tiling WM