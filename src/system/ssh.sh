#!/bin/bash
setupNewKey=$(comfirmStep "SSH KeyGen" "Do You Already Have An SSH Key?" )

if [[ $setupNewKey == 0 ]]; then
    echo "You already have an SSH Key"
else 
    echo "Creating a new SSH Key"
    ssh-keygen -t RSA -b 2048 -C "$($ComputerModel)" -P "" -f $HOME/.ssh/id_rsa
    xclip -sel clip ~/.ssh/id_rsa.pub
    whiptail --title "SSH Key" --msgbox "Your new SSH Key as been copied to your clipboard" 10 20
fi