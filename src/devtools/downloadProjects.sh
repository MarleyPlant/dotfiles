
    # --- Clone All Public Git Repositories ---
    mkdir ~/Projects
    cp $DTFCfgDir/configs/gitlab-icon.png /usr/share/icons/gitlab-icon.png

    gio set Projects -t string metadata::custom-icon "file:///usr/share/icons/gitlab-icon.png"

    cd ~/Projects
    bash $HOME/.scripts/cloneallgit
    cd $DTFDir