#!/bin/bash
allpackages=$(get_array_from_json $DTFCfgDir/applications.json ".apt[]")
packages=($(multiselect "$allpackages" "APT Packages"))

for package in "${packages[@]}"; do
    packageName=$(echo $package | jq -r '.package')
    setupScript=$(echo $package | jq -r 'try .setupscript ')
    if [ "$setupScript" != "null" ]; then
        eval "$DTFCfgDir/applications/setup/$setupScript/pre.sh"
    fi
    install_apt_packages $packageName
    if [ "$setupScript" != "null" ]; then
        eval "$DTFCfgDir/applications/setup/$setupScript/post.sh"
    fi
done
