#!/bin/bash

# -------------------------- xpi tools ---------------------------------

get_addon_id_from_xpi () { #path to .xpi file
    addon_id=`unzip -p "$1" "manifest.json" | jq -r '.applications[].id'`
    echo "$addon_id"
}

# Installs .xpi given by relative path
# to the extensions path given
install_addon () {
    ADDONURL=`get_addon_url $1`
    echo "Installing addon $1"
    wget -q $ADDONURL
    filename=`basename $ADDONURL`
    xpi="${PWD}/${filename}"
    FIREFOXPROFILE=`find ~/.mozilla/firefox/*.default* | head -n 1`
    extensions_path="${FIREFOXPROFILE}/extensions/"
    if [ -f "$extensions_path" ]; then
            echo "Extensions Folder Found"
    else
            mkdir $extensions_path
    fi

    new_filename=`get_addon_id_from_xpi $xpi`.xpi
    new_filepath="${extensions_path}${new_filename}"
    if [ -f "$new_filepath" ]; then
        echo "Addon Already Installed: $1"
        rm -f "$xpi"
    else
        mv "$xpi" "$new_filepath"
    fi
}

get_addon_url () {
    APIURL="https://addons.mozilla.org/api/v5/"
    ENDPOINT="addons/addon/"
    CALL="${APIURL}${ENDPOINT}${1}/"
    URL=`curl -s $CALL | jq -r '.current_version.file.url'`
    echo "$URL"
}



# Install Firefox Extensions
extensions=($(get_array_from_json ./configs/applications/config/firefox/extensions.json ".addons[]"))
for extension in "${extensions[@]}"; do
    install_addon $extension
done
