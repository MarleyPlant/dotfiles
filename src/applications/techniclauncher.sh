#!/bin/bash

## Download & Installs Tekkit Launcher Program

wget http://mirror.technicpack.net/Technic/technic-launcher.jar;

sudo mv ./technic-launcher.jar /usr/games

sudo chmod -x /usr/games/technic-launcher.jar;

sudo chmod 777 /usr/games/technic-launcher.jar;

 

## Download & Installs Tekkit Icon

sudo cp ../configs/technic/technic-icon.png /usr/share/icons;

 

## Builds & Installs Tekkit Launcher Shortcut

sudo cp ../configs/applications/technic/tekkit_launcher.desktop /usr/share/applications;

sudo chmod -x /usr/share/applications/tekkit_launcher.desktop;

sudo chmod 777 /usr/share/applications/tekkit_launcher.desktop;

