#!/bin/bash
WHIPHEIGHT=$(tput lines)
WHIPHEIGHT=$(($WHIPHEIGHT-2))
WHIPWIDTH=100
DTFDir=$(pwd)
DTFCfgDir=${DTFDir}/configs

install_apt_packages() {
    sudo apt-get install -y -qq $1 $2 $3 $4 $5 $6 $7 $8
}

comfirmStep() {
    whiptail --title "$1" --yesno "$2" $WHIPHEIGHT $WHIPWIDTH 3>&1 1>&2 2>&3; echo $?
}

get_array_from_json() {
    json=$(jq -r -c $2 $1)
    echo $json
}


getMultiLineInput() {
    whiptail --title "$1" --textbox ~/.ssh/id_rsa.pub $WHIPHEIGHT $WHIPWIDTH 3>&1 1>&2 2>&3; echo $?
}

multiselect() {
    options=($1)
    local -A valmappings
    choices=()
    for choice in "${options[@]}"; do
        choiceName=$(echo $choice | jq -r '.name')
        choiceEnabled=$(echo $choice | jq -r 'try .enabled ')
        status="OFF"
        if [ "$choiceEnabled" != "null" ]; then
            status="ON"
        fi
        choices+=($choiceName "" $status)
        valmappings["$choiceName"]="$choice"
    done

    selected=$(
        whiptail --title="$2" --checklist "Select options:" $WHIPHEIGHT $WHIPWIDTH $(($WHIPHEIGHT / 2)) "${choices[@]}" \
            3>&2 2>&1 1>&3-
    )

    for mapping in "${valmappings[@]}"; do
        choiceName=$(echo $mapping | jq -r '.name')
        if [[ ${selected[@]} =~ $choiceName ]]; then
            echo $mapping
        fi
    done

}
