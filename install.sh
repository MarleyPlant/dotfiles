#!/bin/bash
source ./src/util.sh
WELCOMEPROMPT="Marley Plant - Linux Installer Script 
https://marleyplant.com
https://gitlab.com/MarleyPlant \n
This script will install all the necessary packages and configurations for your system. 
Do you want to continue?"

response=$(comfirmStep "Welcome" "$WELCOMEPROMPT")

if [[ $response == 0 ]]; then

    $ComputerModel: $(sudo dmidecode | grep -A3 '^System Information' | grep "Product Name:" | cut -d ":" -f2)

    # --- Upgrade and Install Packages ---

    sudo apt update -y
    sudo apt-get upgrade -y
    install_apt_packages wget gpg curl xclip

    allSteps=$(get_array_from_json $DTFCfgDir/steps.json ".steps[]")
    stepsToExecute=($(multiselect "$allSteps" "What Do You Want To Do?"))

    for step in "${stepsToExecute[@]}"; do
        stepName=$(echo $step | jq -r '.name')
        script=$(echo $step | jq -r 'try .script ')
        if [ "$script" != "null" ]; then
            source ./src/$script
        fi
    done

    # --- OUTPUT INSTALLATION INFORMATION
    neofetch
    echo ""
    echo "Your installation was successful!!"
    echo ""

else
    echo "Installation aborted."
    exit 1
fi
