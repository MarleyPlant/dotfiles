# 🔌 Applications
The Script Allows You To Install All Of The Folllowing Applications and Configurations.

You Can Manually select which options you want or choosse one of my [profiles(coming soon)](/configs/profiles/)

## 🏢 Office
- Firefox
  - [Installer](/src/applications/browser.sh)
  - [Extensions](/configs/applications/config/firefox/extensions.json)
- Thunderbird (Managed With APT Package Installer)
- Obsidian (Managed With Snap Package Installer)

## 👨‍💻 Development
- [Tabby Terminal](/configs/applications/config/tabby/)
- [VSCode](/configs/applications/config/vscode/)
- [Docker](/src/devtools/docker.sh)
- [NVM](/src/devtools/nvm.sh)
- GIT
  - [SSH KEY](/src/system/ssh.sh)
  - [Auto Clone All Projects](/src/devtools/downloadProjects.sh)

## 💻 System
- [POPOS System Preferences](/src/system/preferences.sh)
- [Custom Bash Scripts](/configs/scripts/)
- [ULauncher](/src/system/ulauncher.sh)
- Gnome Shell Extensions
- [ZSH + OHMyZSH](/src/system/shell.sh)
- [SyncThing](/configs/applications/setup/syncthing/)
- [Application Installer](/configs/applications.json)
  - [APT Package Installer](/src/apt.sh)
  - [SNAP Package Installer](/src/snap.sh)


## 🎮 Games & Entertainment
- Technic Launcher
- Steam
- Spotify (Todo)


## 🎨 Themeing
- Icon Theme
