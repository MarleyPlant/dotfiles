<div align='center'>
<h1><b>dotfiles</b></h1>
<p>Configuration Script For POPOS12</p>
</div>

![Demo Gif](./.dev/demo.gif)


![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/marleyplant/dotfiles?style=flat-square) ![GitLab Contributors](https://img.shields.io/gitlab/contributors/marleyplant/dotfiles?style=flat) ![Gitlab Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/marleyplant/dotfiles?style=flat-square)  
 
 <br /> 







## 👨‍💻 Development

### 🔌 Installing Dependencies
```bash
curl -S https:&#x2F;&#x2F;marleyplant.com&#x2F;dotfiles | bash
```




 
 <br />


# 🔌 Applications
The Script Allows You To Install All Of The Folllowing Applications and Configurations.

You Can Manually select which options you want or choosse one of my [profiles(coming soon)](/configs/profiles/)

## 🏢 Office
- Firefox
  - [Installer](/src/applications/browser.sh)
  - [Extensions](/configs/applications/config/firefox/extensions.json)
- Thunderbird (Managed With APT Package Installer)
- Obsidian (Managed With Snap Package Installer)

## 👨‍💻 Development
- [Tabby Terminal](/configs/applications/config/tabby/)
- [VSCode](/configs/applications/config/vscode/)
- [Docker](/src/devtools/docker.sh)
- [NVM](/src/devtools/nvm.sh)
- GIT
  - [SSH KEY](/src/system/ssh.sh)
  - [Auto Clone All Projects](/src/devtools/downloadProjects.sh)

## 💻 System
- [POPOS System Preferences](/src/system/preferences.sh)
- [Custom Bash Scripts](/configs/scripts/)
- [ULauncher](/src/system/ulauncher.sh)
- Gnome Shell Extensions
- [ZSH + OHMyZSH](/src/system/shell.sh)
- [SyncThing](/configs/applications/setup/syncthing/)
- [Application Installer](/configs/applications.json)
  - [APT Package Installer](/src/apt.sh)
  - [SNAP Package Installer](/src/snap.sh)


## 🎮 Games & Entertainment
- Technic Launcher
- Steam
- Spotify (Todo)


## 🎨 Themeing
- Icon Theme


 
 <br />


 
 <br />


## 💻 **TECHNOLOGIES**
[![npm](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;npm-CB3837?style&#x3D;for-the-badge&amp;logo&#x3D;npm&amp;logoColor&#x3D;white)]()
[![Composer](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Composer-885630?style&#x3D;for-the-badge&amp;logo&#x3D;Composer&amp;logoColor&#x3D;white)]()
[![Yarn](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Yarn-2C8EBB?style&#x3D;for-the-badge&amp;logo&#x3D;Yarn&amp;logoColor&#x3D;white)]()
[![gulp](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;gulp-CF4647?style&#x3D;for-the-badge&amp;logo&#x3D;gulp&amp;logoColor&#x3D;white)]()
[![Grunt](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Grunt-FAA918?style&#x3D;for-the-badge&amp;logo&#x3D;Grunt&amp;logoColor&#x3D;white)]()
[![Webpack](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Webpack-8DD6F9?style&#x3D;for-the-badge&amp;logo&#x3D;Webpack&amp;logoColor&#x3D;white)]()
[![Babel](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Babel-F9DC3E?style&#x3D;for-the-badge&amp;logo&#x3D;Babel&amp;logoColor&#x3D;white)]()
[![Docker](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Docker-2496ED?style&#x3D;for-the-badge&amp;logo&#x3D;Docker&amp;logoColor&#x3D;white)]()
[![Android Studio](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Android%20Studio-3DDC84?style&#x3D;for-the-badge&amp;logo&#x3D;Android%20Studio&amp;logoColor&#x3D;white)]()


 
 <br />

